# Commandes de Base

Excel est un outil puissant pour la gestion et l'analyse des données. Cette section couvre les commandes de base
essentielles pour tous les utilisateurs d'Excel.

## Saisie et Mise en Forme des Données

### Vue d'Ensemble

La saisie et la mise en forme des données sont la base de l'utilisation d'Excel. Cette section vous guidera à travers
diverses techniques de saisie de données et méthodes de mise en forme pour une meilleure lisibilité et efficacité.

### Étapes pour la Saisie de Données

1. **Sélectionnez une cellule** : Cliquez sur la cellule où vous souhaitez entrer des données.
    1. Les données peuvent être de plusieurs types, y compris des nombres, du texte, des dates, des heures, monétaires,
       etc.
2. **Entrez les données** : Tapez les données et appuyez sur 'Entrée' ou 'Tab' pour passer à la cellule suivante.

 ### LES FORMULES

Les formules sont des équations qui effectuent des calculs sur les valeurs de la feuille de calcul. Une formule peut se composer de valeurs, de cellules, d'opérateurs et de fonctions. Les formules peuvent être simples ou complexes selon les besoins de l'utilisateur. Les formules sont entrées dans la barre de formule et commencent toujours par un signe égal (=).

## Formules de Base
 

### Formule d SOMME

- **Utilisation** : Pour additionner des nombres dans une plage.
- **Syntaxe** : `=SOMME(nombre1, [nombre2], ...)`
- **Exemple** : `=SOMME(A1:A10)` additionne tous les nombres de la cellule A1 à A10.

### Formule MOYENNE

- **Utilisation** : Pour calculer la moyenne de nombres dans une plage.
- **Syntaxe** : `=MOYENNE(nombre1, [nombre2], ...)`
- **Exemple** : `=MOYENNE(B1:B10)` calcule la moyenne des nombres de B1 à B10.


# Formatage Conditionnel dans Excel

Le formatage conditionnel dans Excel permet de changer l'apparence des cellules en fonction des conditions que vous définissez. Voici comment l'utiliser :

## Accéder au Formatage Conditionnel

1. **Sélectionnez les Cellules à Formater** : Cliquez et faites glisser votre souris pour sélectionner les cellules que vous souhaitez formater.

2. **Allez dans l'Onglet 'Accueil'** : Cliquez sur l'onglet 'Accueil' dans le ruban en haut de l'interface Excel.

3. **Choisissez 'Formatage Conditionnel'** : Dans le groupe 'Style', vous trouverez l'option 'Formatage Conditionnel'. Cliquez dessus pour voir les différentes options de formatage.
![Menu  Format Conditionnel](../images/conditional_formating.png "Conditional Formating")## Appliquer un Formatage Conditionnel

### Règles de Mise en Surbrillance des Cellules

- **Utilisation** : Mettre en évidence les cellules selon des critères spécifiques (par exemple, cellules contenant des valeurs supérieures à un certain seuil).
- **Comment faire** : 
  1. Sélectionnez 'Règles de Mise en Surbrillance des Cellules'.
  2. Choisissez une règle (par exemple, 'Supérieur à').
  3. Entrez la valeur seuil et choisissez un format.
  4. Cliquez sur 'OK'.

### Barres de Données, Nuances de Couleur et Jeux d'icônes

- **Utilisation** : Visualiser les données à l'aide de barres de données, de nuances de couleur ou d'icônes.
- **Comment faire** : 
  1. Choisissez 'Barres de Données', 'Nuances de Couleur' ou 'Jeux d'icônes'.
  2. Sélectionnez le style souhaité.
  3. Les formats seront appliqués automatiquement en fonction des données.

### Créer des Règles Personnalisées

- **Utilisation** : Définir des règles spécifiques qui ne sont pas couvertes par les options prédéfinies.
- **Comment faire** : 
  1. Sélectionnez 'Nouvelle Règle'.
  2. Choisissez le type de règle.
  3. Définissez les critères et le format.
  4. Cliquez sur 'OK'.

### Gérer les Règles

- **Utilisation** : Modifier ou supprimer des règles existantes.
- **Comment faire** :
  1. Sélectionnez 'Gérer les Règles'.
  2. Vous pouvez alors modifier ou supprimer des règles existantes.

## Exemples d'Application

- **Mise en évidence des scores élevés** : Utilisez 'Supérieur à' pour mettre en évidence les scores au-dessus de 80 dans une liste d'examens.
- **Analyse des tendances de vente** : Utilisez des barres de données pour visualiser les volumes de vente.

Le formatage conditionnel est un outil qui aide à améliorer la compréhension visuelle de vos données,

()